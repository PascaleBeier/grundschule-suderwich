<?php

/**
 * Extension Manager/Repository config file for ext "grundschule_suderwich".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'Grundschule Suderwich',
    'description' => 'Sitepackage der Grundschule Suderwich',
    'category' => 'templates',
    'constraints' => [
        'depends' => [
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'Pascalebeierde\\GrundschuleSuderwich\\' => 'Classes',
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Pascale Beier',
    'author_email' => 'mail@pascalebeier.de',
    'author_company' => 'pascalebeier.de',
    'version' => '1.0.1',
];
