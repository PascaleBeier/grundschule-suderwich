<?php
defined('TYPO3_MODE') || die();

///***************
// * Add default RTE configuration
// */
//$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['grundschule_suderwich'] = 'EXT:grundschule_suderwich/Configuration/RTE/Default.yaml';
//
///***************
// * PageTS
// */
//\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig('<INCLUDE_TYPOSCRIPT: source="FILE:EXT:grundschule_suderwich/Configuration/TsConfig/Page/All.tsconfig">');

$GLOBALS['TYPO3_CONF_VARS']['FE']['ContentObjects'] = array_merge(
   $GLOBALS['TYPO3_CONF_VARS']['FE']['ContentObjects'],
   [
      'FILE' => \TYPO3\CMS\Frontend\ContentObject\FileContentObject::class,
   ],
);
